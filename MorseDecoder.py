import time
from MorseAlphabet import alphabet

class MorseDecoder:
    def __init__(self, light, dotTime=0.2):
        self.light = light
        self.roomLighting = 0
        self.threshold = 2000
        self.dotTime = dotTime
        self.dashTime = self.dotTime * 3
        self.spaceTime = self.dotTime * 7
        self.signalTimeout = self.dotTime * 9
        self.signalFound = False;
    
    # Get light sensor status (On=True - Off=False)
    #
    def getLight(self):
        val = self.light.value
        if (val > self.roomLighting + self.threshold):
            return True
        else:
            return False

    # Convert morse string into text string
    #
    def convert(self, string):
        result = ""
        morseWords = string.split("%")
        for morseWord in morseWords:
            morseCodes = morseWord.split(" ")
            for morseCode in morseCodes:
                for x in alphabet:
                    if x[1] == morseCode:
                        result += x[0]
                result += ""
            result += " "
        return result
    
    def isDotTime(self, t):
        return t == self.dotTime

    def isDashTime(self, t):
        return t == self.dashTime

    def isSpaceTime(self, t):
        return t == self.spaceTime

    def isTransmissionHover(self, startOFFtime):
        return (time.monotonic() - startOFFtime) >= self.signalTimeout

    def findSignal(self):
        if(self.signalFound == False and self.getLight()):
            self.signalFound = True
            print('DECODER_SIGNAL_FOUND')


    # Display Morse code and Text String
    #
    def display(self, code=".-.-.- "):
        print("morse result : " + code)
        print("\nMorse Translation : " + self.convert(code))
    

    def decode(self):
        self.signalFound = False;
        self.roomLighting = self.light.value

        startONtime = 0
        startOFFtime = 0
        lightAcq = False

        print('Base room luminosity value : ' + str(self.roomLighting))
        print('Signal must be greater than: ' + str(self.roomLighting + self.threshold) + '\n')
        print('Waiting for signal... \n')
        
        result = ""
        while(True):
            
            self.findSignal()
            if(self.signalFound):

                # Operations to do when light turn ON
                #
                if(self.getLight() and lightAcq == False):
                    startONtime = time.monotonic()
                    lightAcq = True

                    endOFFtime = time.monotonic()
                    t = round(endOFFtime-startOFFtime, 1)
                    
                    if(self.isDashTime(t)):
                        result += ' '
                    if(self.isSpaceTime(t)):
                        result += '%'

                
                # Operations to do when light turn OFF
                #
                elif(self.getLight() == False and lightAcq == True):
                    endONtime = time.monotonic()
                    t = round(endONtime-startONtime, 1)

                    if(self.isDotTime(t)):
                        result += '.'
                    if(self.isDashTime(t)):
                        result += '-'
                    lightAcq = False
                    startOFFtime = time.monotonic()

                # finish acquisition if no light for a moment 
                elif(lightAcq == False and self.isTransmissionHover(startOFFtime)):
                    break;
        
        return self.convert(result)