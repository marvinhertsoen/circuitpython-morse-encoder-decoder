import time
from MorseAlphabet import alphabet

class MorseFlasher:
    def __init__(self, pixels, color=(255, 0, 0)):
        self.brightness = 0.2
        self.pixels = pixels
        self.dotLength = 0.2
        self.dashLength = self.dotLength * 3
        self.symbolGap = self.dotLength
        self.characterGap = self.dotLength * 3
        self.wordGap = self.dotLength * 7
        
        self._color = (
            int(color[0] * self.brightness),
            int(color[1] * self.brightness),
            int(color[2] * self.brightness)
        )
    
    # Show light for certain time
    #
    def showLight(self, onTime=0, offTime=0):
        # show light for t time
        self.pixels.fill(self._color)
        self.pixels.show()
        time.sleep(onTime)        
        # then turn off light for symbol gap time
        self.pixels.fill((0, 0, 0))
        self.pixels.show()
        time.sleep(offTime)
    
    # Convert string into morse symbols
    #
    def encode(self, string):
        result = ""
        for char in string:
            if char == " ":
                result += "%"
            else:
                for x in alphabet:
                    if x[0] == char:
                        result += x[1]
                result += " "
        self.display(result)
    
    # Display morse sequence with light
    #
    def display(self, code="... --- ..."):
        # iterate through morse code symbols
        for c in code:
            # show a dot
            if c == ".":
                self.showLight(self.dotLength, self.symbolGap)
            # show a dash
            elif c == "-":
                self.showLight(self.dashLength, self.symbolGap)
            # show a gap
            elif c == " ":
                time.sleep(self.characterGap)
            elif c == "%":
                time.sleep(self.wordGap)
    
