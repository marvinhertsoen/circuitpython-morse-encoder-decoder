#!/usr/bin/env python3

import argparse
import os, sys
from PySide2.QtCore import Property, QUrl, QObject, Slot, Signal, QSocketNotifier
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtGui import QGuiApplication, QColor
import serial

class Backend(QObject):

    #Signals
    buttonPressed = Signal()
    reactivateButtons = Signal()
    resetEncodeButton = Signal()
    busyEncodeButton = Signal()
    resetDecodeButton = Signal()
    busyDecodeButton = Signal()
    readingDecodeButton = Signal()
    textDecodedChanged = Signal()
    

    def __init__ (self, parent=None):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument("-p", "--port", help="the serial port", default='/dev/ttyACM0')
        self.args = self.parser.parse_args()
        QObject.__init__(self, parent)
        self.port = serial.Serial(self.args.port, timeout=1)
        self.port_file = QSocketNotifier(self.port.fileno(), QSocketNotifier.Read)
        self.port_file.activated.connect(self.portReadyRead)
        self.text_decoded = ""

    @Slot()
    def portReadyRead(self):
        data = self.port.readline().strip()
        print(data)

        if data == b"OK_ENCODING":
            print("Starting encoding...")
            self.busyEncodeButton.emit()

        elif data == b"OK_ENCODED":
            print("Encoding finished")
            self.resetEncodeButton.emit()
            self.reactivateButtons.emit()

        elif data == b"OK_DECODING":
            print("Waiting for SIGNAL...")
            self.busyDecodeButton.emit()

        elif data == b"DECODER_SIGNAL_FOUND":
            print("Signal found, reading...")
            self.readingDecodeButton.emit()

        elif data.startswith(b"OK_DECODED="):
            print("Decoding finished")
            text = data.decode('utf-8')
            self.text_decoded = text.replace("OK_DECODED=", "")
            self.textDecodedChanged.emit()
            self.resetDecodeButton.emit()
            self.reactivateButtons.emit()

    @Slot(str)
    def hello(self, text):
        print("Hello from python: %s" % text)

    @Slot(str)
    def encode(self, text):
        print('encoding..')
        self.port.write(b"ENCODE=%s\r" % text.encode("ascii"))

    @Slot()
    def decode(self):
        print('decoding..')
        self.port.write(b"DECODE\r")
    
    def _textDecoded(self):
        return self.text_decoded

    textDecoded = Property(str, _textDecoded, notify = textDecodedChanged)


if __name__ == '__main__':
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()

    backend = Backend()
    engine.rootContext().setContextProperty("backend", backend)

    view_file = os.path.join(os.path.dirname(__file__), 'view.qml')
    engine.load(QUrl.fromLocalFile(view_file))
    app.exec_()

