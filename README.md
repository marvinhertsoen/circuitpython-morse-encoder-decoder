# CircuitPython Morse Encoder-Decoder

A simple implementation of a light-based MORSE Encoder/Decoder made for Playground Express with passion and a lot of <3.

### Overview

Here the features :
* Light morse decoding
* Light morse encoding
* UI

### Overview
**Setup the project**

Make sure your Playground Express is connected

I used python 3.6 to develop this software.

install PySide2 and pyserial :
```
pip install PySide2
pip install pyserial
```



Copy these files/folders on the Playground Express:
* lib/
* code.py
* main.py
* __init__.py
* MorseAlphabet.py
* MorseDecoder.py
* MorseFlasher.py
* view.qml

**Launch the project**
```
python ./app.py
```

You can also set the target serial port 
```
python ./app.py -p '/dev/ttyACM0'
```

### Notes
the default serial port is : "/dev/ttyACM0"

Morse Decoding :
Default flash dot frequency = 0.2s (I used the 'Flashlight' Android app)

