import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Dialogs 1.0

ApplicationWindow {

	width: 800
	height: 600
	visible: true
	Column {
		anchors.centerIn: parent

		Row {
			bottomPadding: 30

			Label {
				horizontalAlignment: Text.AlignVCenter
				text: "Morse Flash Decoder/Encoder"
			    font.family: "Helvetica"
    			font.pointSize: 24
			}
		}
		
		Row {
			spacing: 10
			bottomPadding: 20

			Button {
				id: encodeButton
				text: "Encode"
				onClicked: function() {
					backend.encode(encodeInput.text);
					decodeButton.enabled = false;
					encodeButton.enabled = false;
				}
			}

			TextField {
				id: encodeInput
 				text: "SOS"
			}
		}

		Row {
			spacing: 10
			bottomPadding: 20

			Button {
				id: decodeButton
				text: "Decode"
				onClicked: function() {
					backend.decode();
					decodeButton.enabled = false;
					encodeButton.enabled = false;
				}
				enabled: true
			}
			TextField {
				id: decodeInput
 				text: backend.textDecoded
				readOnly: true
			}
		}

		Row {
			Button {
				text: "Quit"
				onClicked: function() {
					Qt.quit()
				}
			}
		}

	}

	Connections {
		target: backend
		onReactivateButtons: {
			encodeButton.enabled = true
			decodeButton.enabled = true
		}

		onResetEncodeButton: {
			encodeButton.text = "Encode"
		}
		onBusyEncodeButton: {
			encodeButton.text = "Encoding..."
		}
		onResetDecodeButton: {
			decodeButton.text = "Decode"
		}
		onBusyDecodeButton: {
			decodeButton.text = "Waiting Signal"
		}
		onReadingDecodeButton: {
			decodeButton.text = "Signal Acquisition..."
		}
	}
}

