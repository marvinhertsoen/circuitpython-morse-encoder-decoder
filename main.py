import supervisor

import board
import neopixel
from MorseFlasher import MorseFlasher
from MorseDecoder import MorseDecoder
import os
import analogio
import time

COLOR = (255,128,20)

# Initialize NeoPixels
pixels = neopixel.NeoPixel(board.NEOPIXEL, 10, auto_write=False)
pixels.fill((0, 0, 0))
pixels.show()

# Initialize the light sensor
lightSensor = analogio.AnalogIn(board.LIGHT)

# Create morse flasher object.
flasher = MorseFlasher(pixels, COLOR)
# Create morse decoder object.
decoder = MorseDecoder(lightSensor, 0.2)

# Clear python cli
#
def clear():
    print(chr(27)+'[2j')
    print('\033c')
    print('\x1bc')

def init_state():
    print("wait")

while True:

    if not(supervisor.runtime.serial_connected):
        init_state()
        continue

    if supervisor.runtime.serial_bytes_available:
        cmd = input().strip()
        if cmd.startswith(b"ENCODE="):
            print("OK_ENCODING")
            flasher.encode(cmd.replace("ENCODE=", "").upper())
            print("OK_ENCODED")

        elif cmd == "DECODE":
            print("OK_DECODING")
            text = decoder.decode()
            print("OK_DECODED=%s" % text)
        else:
            print("ERR:{%s}" % cmd)

